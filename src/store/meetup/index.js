
import * as firebase from 'firebase'

export default {
    state: {
        loadedMeetups: [
            { imageUrl: 'https://media-cdn.tripadvisor.com/media/photo-s/0e/9a/e3/1d/freedom-tower.jpg' , 
            id: 'dgfdf832565fdfdfdsa' , 
            title: 'Meetups in Newyourk' , 
            date: new Date() ,
            location: 'America' ,
            description: 'Nice palce..'},

            { imageUrl: 'https://en.parisinfo.com/var/otcp/sites/images/node_43/node_51/node_233/visuel-dossier-rentr%C3%A9e-culturelle-2017-%7C-740x380-%7C-%C2%A9-dr/18108997-1-fre-FR/Visuel-dossier-Rentr%C3%A9e-culturelle-2017-%7C-740x380-%7C-%C2%A9-DR.jpg' , 
            id: 'dss56565fdfdfdsa' , 
            title: 'Meetups in Paris' , 
            date: new Date()  ,
            location: 'Paris' ,
            description: 'Nice palce..toooo'},
        ]
    },
    mutations: {
  

        updateMeetup(state , payload){
            const meetup = state.loadedMeetups.find(
                meetup => {
                    return meetup.id === payload.id
                })
                if(payload.title){
                    meetup.title = payload.title
                }
                if(payload.description){
                    meetup.description = payload.description
                }
                if(payload.date){
                    meetup.date = payload.date
                }
        },

        setLoadedMeetups(state , payload){
          state.loadedMeetups = payload
        },

        createMeetup (state , payload){
            state.loadedMeetups.push(payload)
        }

    },
    actions: {

        
        loadMeetups({commit}){
            commit('setLoading' , true)
            firebase.database().ref('meetups').once('value')
            .then(
                (data) => {
                    const meetups = []
                    const obj = data.val()
                    for(let key in obj){
                        meetups.push({
                            id: key,
                            title: obj[key].title,
                            location: obj[key].location,
                            description: obj[key].description,
                            imageUrl: obj[key].imageUrl,
                            date: obj[key].date,
                            creatorId: obj[key].creatorId
                        })
                    }
                    
                    commit('setLoadedMeetups' , meetups)
                    commit('setLoading' , false)
                }
            )
            .catch(
                (error) => {
                    console.log(error)
                    commit('setLoading' , true)
                }
            )
        },

        createMeetup ({commit , getters} , payload){
            const meetup = {
                title: payload.title,
                location: payload.location,
                description: payload.description,
                date: payload.date.toISOString() ,
                creatorId: getters.user.id
            }
            let imageUrl
            let key
            //reach out to firebase and store it
            firebase.database().ref('meetups').push(meetup)
            .then((data) => {
                 key = data.key
                 return key
            })
            .then(key => {
                const filename = payload.image.name
                const ext = filename.slice(filename.lastIndexOf('.'))
                return firebase.storage().ref('meetups/' + key + '.' + ext).put(payload.image)
            })
            .then(fileData => {
                imageUrl = fileData.metadata.downloadURLs[0]
                return firebase.database().ref('meetups').child(key).update({imageUrl: imageUrl})
            })
            .then(() => {
                commit('createMeetup' , {
                    ...meetup,
                    imageUrl: imageUrl,
                    id: key
                })
            })
            .catch((error) => { 
                console.log(error)
            })
            

        },

        updateMeetupData({commit} , payload){
            commit('setLoading' , true)
            const updateObj = {}
            if(payload.title){
                updateObj.title = payload.title
            }
            if(payload.description){
                updateObj.description = payload.description
            }
            if(payload.date){
                updateObj.date = payload.date
            }
            firebase.database().ref('/meetups').child(payload.id).update(updateObj)
            .then(() => {
                commit('setLoading' , false)
                commit('updateMeetup' , payload)
            })
            .catch(
                error => {
                    console.log(error)
                    commit('setLoading' , false)
                }
            )

        }
    },
    getters: {

        loadedMeetups(state){
            return state.loadedMeetups.sort((meetupA , meetupB) => {
                return meetupA.date > meetupB.date
            })
        },

        featuredMeetups(state , getters){
            
            return getters.loadedMeetups.slice(0,5)
        },

        loadedMeetup(state){
            return (meetupId) => {
                return state.loadedMeetups.find((meetup) => {
                   return meetup.id === meetupId 
                })
            }
        }
    }
}
