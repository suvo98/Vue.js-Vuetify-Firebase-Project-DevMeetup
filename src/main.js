
import Vue from 'vue'

import Vuetify from 'vuetify'
import './stylus/main.styl'

import * as firebase from 'firebase'

import App from './App'
import router from './router'
import { store } from './store'
import DateFilter from './filter/date'

import AlertCmp from './components/shared/Alert.vue'
import EditMeetupDetailse from './components/Meetup/Edit/EditMeetupDetailse.vue'
import EditMeetupDate from './components/Meetup/Edit/EditMeetupDate.vue'
import EditMeetupTime from './components/Meetup/Edit/EditMeetupTime.vue'
import RegisterDialogue from './components/Meetup/Registered/RegisterDialogue.vue'



Vue.use(Vuetify)

Vue.config.productionTip = false

Vue.filter('date' , DateFilter)

Vue.component('app-alert' , AlertCmp)
Vue.component('app-edit-meetups-details-dialogue' , EditMeetupDetailse)
Vue.component('app-edit-meetups-date-dialogue' , EditMeetupDate)
Vue.component('app-edit-meetups-time-dialogue' , EditMeetupTime)
Vue.component('app-meetups-register-dialogue' , RegisterDialogue)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created(){
    firebase.initializeApp({
      apiKey: 'AIzaSyDFizMgWQoc-e6SXxVNwwnmER7KLQs7xkc',
      authDomain: 'vuejs-vuetify-meetup.firebaseapp.com',
      databaseURL: 'https://vuejs-vuetify-meetup.firebaseio.com',
      projectId: 'vuejs-vuetify-meetup',
      storageBucket: 'gs://vuejs-vuetify-meetup.appspot.com'
    })
    firebase.auth().onAuthStateChanged(
      (user) => {
        if(user){
          this.$store.dispatch('autoSignIn' , user)
          this.$store.dispatch('fetchUserData')
          
        }
     })
    this.$store.dispatch('loadMeetups')
  }
})
