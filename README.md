# MeetUp
This app help you to meetup with your friends with location image. Time details. Also include a CRUD functionality.
Secure, Reliable, Speedy Accessible.



## Server Side Technology
1. Firebase


## File Uploading Technology
1. Upload File In NoSQL database (firebase)
2. Store the file in Google Firebase server.



## Client Side Technology
1. VueJS
2. Vuex
3. VueTiFY

## Online Access
https://rocky-headland-34355.herokuapp.com


